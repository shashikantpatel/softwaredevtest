﻿using System;
using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {

        [Test]
        public void Statement_WhenStatementGenerated()
        {
            //Arrange
            var expected =
                "Statement for Henry\r\n\nSharpBank.CheckingAccount\r\n  deposit $100.00\r\nAccountBalanceTotal $100.00\n\nSharpBank.SavingsAccount\r\n  deposit $4,000.00\r\n  withdrawal $200.00\r\nAccountBalanceTotal $3,800.00\n\nTotal In All Accounts $3,900.00";
            CheckingAccount checkingAccount = new CheckingAccount();
            SavingsAccount savingsAccount = new SavingsAccount();

            Customer henry = new Customer("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount);

            //Act
            checkingAccount.Deposit(100.00M);
            savingsAccount.Deposit(4000.00M);
            savingsAccount.Withdraw(200.00M);

            var result = henry.GetStatement();

            //Assert
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void WithDrawal_WhenBalanceIsLessThanAmountWithdrawn()
        {
            //Arrange
            var savingsAccount = new SavingsAccount();
            Customer henry = new Customer("Henry").OpenAccount(savingsAccount);

            var expected = "Withdrawal amount is greater than balance";

            savingsAccount.Deposit(400.00m);
            savingsAccount.Deposit(100.00m);

            //Act
            var ex = Assert.Throws<ArgumentException>(() => savingsAccount.Withdraw(2000.00m));

            //Assert
            Assert.That(ex.Message, Is.EqualTo(expected));
        }

        [Test]
        public void Withdraw_WhenAmountIsZero()
        {
            //Arrange
            var savingsAccount = new SavingsAccount();
            Customer henry = new Customer("Henry").OpenAccount(savingsAccount);

            var expected = "Withdrawal amount must be greater than zero";

            savingsAccount.Deposit(400.00m);
            savingsAccount.Deposit(100.00m);

            //Act
            var ex = Assert.Throws<ArgumentException>(() => savingsAccount.Withdraw(0));

            //Assert
            Assert.That(ex.Message, Is.EqualTo(expected));
        }

        [Test]
        public void Withdraw_WhenAmountIsLessThanZero()
        {
            //Arrange
            var savingsAccount = new SavingsAccount();
            Customer henry = new Customer("Henry").OpenAccount(savingsAccount);

            var expected = "Withdrawal amount must be greater than zero";

            savingsAccount.Deposit(400.00m);
            savingsAccount.Deposit(100.00m);

            //Act
            var ex = Assert.Throws<ArgumentException>(() => savingsAccount.Withdraw(-50));

            //Assert
            Assert.That(ex.Message, Is.EqualTo(expected));
        }

        [Test]
        public void Withdraw_WhenIncorrectAmountWithdrawn()
        {
            //Arrange
            var savingsAccount = new SavingsAccount();
            var expected = 300;

            Customer henry = new Customer("Henry").OpenAccount(savingsAccount);
            savingsAccount.Deposit(400.00m);
            savingsAccount.Deposit(100.00m);

            savingsAccount.Withdraw(200);


            //Act
            var result = savingsAccount.AccountBalanceTotal();

            //Assert
            Assert.AreEqual(result, expected);
        }

        [Test]
        public void Deposit_WhenAmoutIsZero()
        {
            //Arrange
            var savingsAccount = new SavingsAccount();
            Customer henry = new Customer("Henry").OpenAccount(savingsAccount);

            var expected = "Deposit amount must be greater than zero";

            savingsAccount.Deposit(400.00m);
            savingsAccount.Deposit(100.00m);

            //Act
            var ex = Assert.Throws<ArgumentException>(() => savingsAccount.Deposit(0));

            //Assert
            Assert.That(ex.Message, Is.EqualTo(expected));
        }

        [Test]
        public void Deposit_WhenAmoutIsLessThanZero()
        {
            //Arrange
            var savingsAccount = new SavingsAccount();
            Customer henry = new Customer("Henry").OpenAccount(savingsAccount);

            var expected = "Deposit amount must be greater than zero";

            savingsAccount.Deposit(400.00m);
            savingsAccount.Deposit(100.00m);

            //Act
            var ex = Assert.Throws<ArgumentException>(() => savingsAccount.Deposit(-50));

            //Assert
            Assert.That(ex.Message, Is.EqualTo(expected));
        }

        [Test]
        public void Deposit_WhenAmountIsInCorrect()
        {
            //Arrange
            var savingsAccount = new SavingsAccount();
            Customer henry = new Customer("Henry").OpenAccount(savingsAccount);

            var expected = "Incorrect Amount after Deposit";

            savingsAccount.Deposit(400.00m);
            savingsAccount.Deposit(100.00m);

            //Act
            savingsAccount.Deposit(200);

            //Assert
            Assert.AreEqual(savingsAccount.AccountBalanceTotal(), 700, expected);
        }

        [Test]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void OpenAccounts_WhenNumberOfAccountsIsMultipleTimes(int numberOfAccounts)
        {
            //Arrange
            Customer oscar = new Customer("Oscar");
            if (numberOfAccounts == 1)
                oscar = oscar.OpenAccount(new SavingsAccount());

            if (numberOfAccounts == 2)
                oscar = oscar.OpenAccount(new SavingsAccount()).OpenAccount(new CheckingAccount());

            if (numberOfAccounts == 3)
                oscar =
                    oscar.OpenAccount(new SavingsAccount())
                        .OpenAccount(new CheckingAccount())
                        .OpenAccount(new MaxiSavingsAccount());

            //Act
            var result = oscar.GetNumberOfAccounts();

            //Assert
            Assert.AreEqual(numberOfAccounts, result);
        }


        [Test]
        public void Transfer_WhenFromAccountIsNull()
        {
            //Arrange
            var savingsAccount = new SavingsAccount();
            var checkingAccount = new CheckingAccount();

            Customer oscar = new Customer("Oscar").OpenAccount(savingsAccount).OpenAccount(checkingAccount);

            var expected = "Value cannot be null.\r\nParameter name: 'From' Account";

            savingsAccount.Deposit(500);

            //Act
            var ex = Assert.Throws<ArgumentNullException>(() => oscar.TransferMoney(null, checkingAccount, 200));

            //Assert
            Assert.That(ex.Message, Is.EqualTo(expected));
        }

        [Test]
        public void Transfer_WhenToAccountIsNull()
        {
            //Arrange
            var savingsAccount = new SavingsAccount();
            var checkingAccount = new CheckingAccount();

            Customer oscar = new Customer("Oscar").OpenAccount(savingsAccount).OpenAccount(checkingAccount);

            var expected = "Value cannot be null.\r\nParameter name: 'To' Account";

            savingsAccount.Deposit(500);

            //Act
            var ex = Assert.Throws<ArgumentNullException>(() => oscar.TransferMoney(savingsAccount, null, 200));

            //Assert
            Assert.That(ex.Message, Is.EqualTo(expected));
        }

        [Test]
        public void Transfer_WhenNegativeAmountGiven()
        {
            //Arrange
            var savingsAccount = new SavingsAccount();
            var checkingAccount = new CheckingAccount();

            Customer oscar = new Customer("Oscar").OpenAccount(savingsAccount).OpenAccount(checkingAccount);

            var expected = "Invalid amount (-200)";

            savingsAccount.Deposit(500);

            //Act
            var ex = Assert.Throws<ArgumentException>(() => oscar.TransferMoney(savingsAccount, checkingAccount, -200));

            //Assert
            Assert.That(ex.Message, Is.EqualTo(expected));
        }

        [Test]
        public void Transfer_WhenToAnotherAccountNotBelongingToCustomer()
        {
            //Arrange
            var savingsAccount = new SavingsAccount();
            var checkingAccount = new CheckingAccount();
            var anotherAccount = new MaxiSavingsAccount();

            Customer oscar = new Customer("Oscar").OpenAccount(savingsAccount).OpenAccount(checkingAccount);

            var expected = "'To' Account does not belong to Customer";

            savingsAccount.Deposit(500);

            //Act
            var ex = Assert.Throws<ArgumentException>(() => oscar.TransferMoney(savingsAccount, anotherAccount, 200));

            //Assert
            Assert.That(ex.Message, Is.EqualTo(expected));
        }

        [Test]
        public void Transfer_WhenFromAnotherAccountNotBelongingToCustomer()
        {
            //Arrange
            var savingsAccount = new SavingsAccount();
            var checkingAccount = new CheckingAccount();
            var anotherAccount = new MaxiSavingsAccount();

            Customer oscar = new Customer("Oscar").OpenAccount(savingsAccount).OpenAccount(checkingAccount);

            var expected = "'From' Account does not belong to Customer";

            savingsAccount.Deposit(500);

            //Act
            var ex = Assert.Throws<ArgumentException>(() => oscar.TransferMoney(anotherAccount, savingsAccount, 200));

            //Assert
            Assert.That(ex.Message, Is.EqualTo(expected));
        }

        [Test]
        public void Transfer_WhenExcessiveWithdrawalFromSavingsAccount()
        {
            //Arrange
            var savingsAccount = new SavingsAccount();
            var checkingAccount = new CheckingAccount();

            Customer oscar = new Customer("Oscar").OpenAccount(savingsAccount).OpenAccount(checkingAccount);

            savingsAccount.Deposit(500);
            checkingAccount.Deposit(500);

            var expected = "Withdrawal amount is greater than balance";

            //Act
            var ex = Assert.Throws<ArgumentException>(() => oscar.TransferMoney(savingsAccount, checkingAccount, 700));

            //Assert
            Assert.That(ex.Message, Is.EqualTo(expected));
        }

        [Test]
        public void Transfer_WhenSuccessfulFromSavingsToCheckingAccount()
        {
            //Arrange
            var savingsAccount = new SavingsAccount();
            var checkingAccount = new CheckingAccount();

            Customer oscar = new Customer("Oscar").OpenAccount(savingsAccount).OpenAccount(checkingAccount);

            savingsAccount.Deposit(500);

            var expectedSavings = 300;
            var expectedChecking = 200;

            //Act
            oscar.TransferMoney(savingsAccount, checkingAccount, 200);

            //Assert
            Assert.AreEqual(savingsAccount.AccountBalanceTotal(), expectedSavings);
            Assert.AreEqual(checkingAccount.AccountBalanceTotal(), expectedChecking);
        }
    }
}

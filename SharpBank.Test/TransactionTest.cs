﻿using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class TransactionTest
    {
        [Test]
        public void Transaction_WhenNewTranscationCreated()
        {
            //Arrange
            var expected = 0.654m;

            //Act
            var transaction = new Transaction(0.654m);

            //Assert
            Assert.AreEqual(expected, transaction.Amount);
        }
    }
}

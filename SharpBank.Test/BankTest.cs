﻿using System;
using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        //private static readonly double DOUBLE_DELTA = 1e-15;

        [Test]
        public void CustomerSummary()
        {
            Bank bank = new Bank();
            Customer john = new Customer("John");
            john.OpenAccount(new CheckingAccount());
            bank.AddCustomer(john);

            Assert.AreEqual("Customer Summary\n - John (1 account)", bank.CustomerSummary());
        }

        [Test]
        public void CheckingAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = new CheckingAccount();
            Customer bill = new Customer("Bill").OpenAccount(checkingAccount);
            bank.AddCustomer(bill);

            checkingAccount.Deposit(100.00M);

            Assert.AreEqual(0.1, bank.TotalInterestPaid());
        }

        [Test]
        public void SavingsAccount()
        {
            //Arrange
            Bank bank = new Bank();
            SavingsAccount savingsAccount = new SavingsAccount();
            bank.AddCustomer(new Customer("Bill").OpenAccount(savingsAccount));
            var expected = .0056m;

            savingsAccount.Deposit(1533);

            //Act
            var result = bank.TotalInterestPaid();

            //Assert
            AssertDiff(expected, result, .01m);
        }

        [Test]
        public void MaxiSavingsAccount()
        {
            //Arrange
            Bank bank = new Bank();
            MaxiSavingsAccount maxiSavingsAccount = new MaxiSavingsAccount();
            bank.AddCustomer(new Customer("Bill").OpenAccount(maxiSavingsAccount));

            //maxiSavingsAccount.Deposit(3000.00m);
            maxiSavingsAccount.Transactions.Add(new Transaction(3000, DateTime.Now.AddDays(-7)));
            var expected = 2.87789m;

            //Act
            var result = bank.TotalInterestPaid();

            //Assert
            AssertDiff(expected, result, .01m);
        }

        [Test]
        public void MaxiSavingsAccountWithNoWithdrawalInLastTenDays()
        {
            //Arrange
            Bank bank = new Bank();
            Account maxiSavingsAccount = new MaxiSavingsAccount();
            bank.AddCustomer(new Customer("Bill").OpenAccount(maxiSavingsAccount));

            maxiSavingsAccount.Transactions.Add(new Transaction(3000.00m, DateTime.Now));
            maxiSavingsAccount.Transactions.Add(new Transaction(3000.00m, DateTime.Now.AddDays(-3)));

            var expected = 2.46m;

            //Act
            var result = bank.TotalInterestPaid();

            //Assert
            AssertDiff(expected, result, .01m);
        }

        [Test]
        public void MaxiSavingsAccount_WithTransactionInLastTenDays()
        {
            //Arrange
            Bank bank = new Bank();
            Account maxiSavingsAccount = new MaxiSavingsAccount();
            bank.AddCustomer(new Customer("Bill").OpenAccount(maxiSavingsAccount));

            maxiSavingsAccount.Transactions.Add(new Transaction(3000.00m, DateTime.Now.AddDays(-7)));
            maxiSavingsAccount.Transactions.Add(new Transaction(-3000.00m, DateTime.Now.AddDays(-3)));
            maxiSavingsAccount.Transactions.Add(new Transaction(50.00m, DateTime.Now.AddDays(-2)));

            var expected = .00096m;

            //Act
            var result = bank.TotalInterestPaid();

            //Assert
            AssertDiff(expected, result, .00001m);
        }


        [Test]
        public void CanGetFirstCustomer()
        {
            Bank bank = new Bank();
            bank.AddCustomer(new Customer("Bill"));
            bank.AddCustomer(new Customer("Ted"));
            bank.AddCustomer(new Customer("Alice"));

            string first = bank.GetFirstCustomer();

            Assert.AreEqual(first, "Bill", "Bank.GetFirstCustomer() mismatch");
        }

        [Test]
        public void CanFailAddNullCustomer()
        {
            Bank bank = new Bank();

            try
            {
                bank.AddCustomer(null);
                Assert.Fail("bank.AddCustomer() did not throw ArgumentNullException when receiving a NULL");
            }
            catch (ArgumentNullException)
            {

            }
        }

        [Test]
        public void Bank_WhenBankManagerCanGenerateCustomerSummary()
        {
            Bank bank = new Bank();
            string sSummary = bank.CustomerSummary();
            Assert.AreEqual(sSummary, "Customer Summary", "No customers - invalid");

            Customer customer = new Customer("Bill");
            bank.AddCustomer(customer);
            sSummary = bank.CustomerSummary();
            Assert.IsTrue(sSummary.ToLower().IndexOf("accounts", StringComparison.Ordinal) >= 0, "1 Customer 0 Accounts - not plural");

            customer.OpenAccount(new CheckingAccount());
            sSummary = bank.CustomerSummary();
            Assert.IsTrue(sSummary.ToLower().IndexOf("accounts", StringComparison.Ordinal) < 0, "1 Account - plural");

            customer.OpenAccount(new SavingsAccount());
            sSummary = bank.CustomerSummary();
            Assert.IsTrue(sSummary.ToLower().IndexOf("accounts", StringComparison.Ordinal) >= 0, "2 Accounts - not plural");
        }

        void AssertDiff(decimal a, decimal b, decimal diff = 0.0001m)
        {
            Assert.That(Math.Abs(a - b), Is.LessThan(diff));
        }
    }
}

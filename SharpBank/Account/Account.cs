﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static System.Math;

namespace SharpBank
{
    public abstract class Account : IAccount
    {
        private readonly object _transactionLock = new object();

        #region Constants
        public const int CHECKING = 0;
        public const int SAVINGS = 1;
        public const int MAXI_SAVINGS = 2;
        #endregion

        #region Properties

        public List<Transaction> Transactions { get; set; }

        #endregion

        #region Constructors
        protected Account()
        {
            Transactions = new List<Transaction>();
        }
        #endregion

        #region Methods
        public virtual void Deposit(decimal amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("Deposit amount must be greater than zero");
            }
            lock (_transactionLock)
            {
                Transactions.Add(new Transaction(amount));
            }
        }

        public virtual void Withdraw(decimal amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("Withdrawal amount must be greater than zero");
            }

            if(amount > AccountBalanceTotal())
            {
                throw new ArgumentException("Withdrawal amount is greater than balance");
            }
            lock (_transactionLock)
            {
                Transactions.Add(new Transaction(-amount));
            }
        }

        public abstract decimal AccountInterestEarned();

        public virtual decimal AccountBalanceTotal()
        {
            return Transactions.Sum(oTransaction => oTransaction.Amount);
        }

        protected decimal CalculateInterest(decimal principle, decimal annualRate, int days)
        {
            decimal dailyRate = annualRate / 365m;

            decimal dailyTotal = principle * (decimal)Math.Pow(1 + (double)dailyRate, days);
            decimal dailyInterest = dailyTotal - principle;

            return dailyInterest;
        }

        protected DateTime FirstTransactionDate()
        {
            var firstOrDefault = Transactions.OrderBy(o => o.TransactionDate).FirstOrDefault();
            if (firstOrDefault != null)
                return firstOrDefault.TransactionDate;
            return DateProvider.GetInstance().Now();
        }

        public virtual string AccountStatement()
        {
            var statement = new StringBuilder();

            statement.AppendLine(ToString());

            var total = 0.0M;

            foreach (var transaction in Transactions)
            {
                var amount = transaction.Amount;

                statement.Append((amount < 0.0M) ? "  withdrawal " : "  deposit ");
                statement.AppendFormat("${0:N2}", Abs(amount));
                statement.Append(Environment.NewLine);

                total += amount;
            }

            statement.AppendFormat("AccountBalanceTotal ${0:N2}", total);

            return statement.ToString();
        }

        #region Unused
        //private double CheckIfTransactionsExist(bool checkAll)
        //{
        //    double amount = 0.0;
        //    foreach (Transaction t in transactions)
        //        amount += t.amount;
        //    return amount;
        //}
        #endregion

        public abstract int AccountType();
        #endregion

        protected bool CheckWithdrawalsInDays(int i)
        {
            return Transactions.Any(t => t.Amount < 0 && (DateProvider.GetInstance().Now().Subtract(t.TransactionDate).TotalDays < i));
        }
    }
}

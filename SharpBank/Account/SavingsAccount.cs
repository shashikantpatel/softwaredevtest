﻿namespace SharpBank
{
    public class SavingsAccount : Account
    {
        #region Methods
        public override int AccountType()
        {
            return SAVINGS;
        }

        public override decimal AccountInterestEarned()
        {
            decimal dAmount = base.AccountBalanceTotal();

            if (dAmount <= 1000)
                return base.CalculateInterest(dAmount, Constants.Rates.SavingsAccount1KRate, 1);
            return base.CalculateInterest(1000.00m, Constants.Rates.SavingsAccount1KRate, 1) + base.CalculateInterest(dAmount - 1000, Constants.Rates.SavingsAccount1KPlusRate, 1);

        }
        #endregion
    }
}

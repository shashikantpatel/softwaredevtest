﻿namespace SharpBank
{
    public interface IAccount
    {
        void Deposit(decimal amount);
        void Withdraw(decimal amount);
        decimal AccountInterestEarned();
        decimal AccountBalanceTotal();
        string AccountStatement();
        int AccountType();
    }
}
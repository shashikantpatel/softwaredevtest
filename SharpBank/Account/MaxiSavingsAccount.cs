﻿using System;

namespace SharpBank
{
    public class MaxiSavingsAccount : Account
    {

        #region Methods
        public override int AccountType()
        {
            return MAXI_SAVINGS;
        }

        public override decimal AccountInterestEarned()
        {
            decimal dAmount = base.AccountBalanceTotal();

            DateTime dFirstTransactionDateTime = FirstTransactionDate();

            int days = (int)DateProvider.GetInstance().Now().Subtract(dFirstTransactionDateTime).TotalDays;
            if (base.CheckWithdrawalsInDays(10))
            {
                return base.CalculateInterest(dAmount, Constants.Rates.MaxiSavingsAccount1KRate, days);

            }
            return base.CalculateInterest(dAmount, Constants.Rates.MaxiSavingsAccount2KPlusRate, days);
        }
        #endregion
    }
}

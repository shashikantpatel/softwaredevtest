﻿namespace SharpBank
{
    public sealed class CheckingAccount : Account
    {
        #region Methods

        public override int AccountType()
        {
            return CHECKING;
        }

        public override decimal AccountInterestEarned()
        {
            var amount = AccountBalanceTotal();

            return amount*Constants.Rates.CheckingAccountRate;
        }

        #endregion
    }
}
﻿namespace SharpBank
{
    public static class Constants
    {
        public static class Accounting
        {
            public const int SignificantDigits = 2;
        }

        public static class Rates
        {
            public const decimal CheckingAccountRate = .001m;

            public const decimal SavingsAccount1KRate = .001m;
            public const decimal SavingsAccount1KPlusRate = .002m;

            public const decimal MaxiSavingsAccount1KRate = .001m;
            public const decimal MaxiSavingsAccount2KRate = .02m;
            public const decimal MaxiSavingsAccount2KPlusRate = .05m;
        }
    }
}

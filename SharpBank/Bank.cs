﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace SharpBank
{
    public class Bank
    {
        #region Properties
        private List<Customer> customers
        {
            get;
            set;
        }
        #endregion

        #region Constructors
        public Bank()
        {
            customers = new List<Customer>();
        }
        #endregion

        #region Methods
        public void AddCustomer(Customer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException("Customer");
            }
            customers.Add(customer);
        }

        public string CustomerSummary()
        {
            string summary = "Customer Summary";
            foreach (Customer customer in customers)
                summary += "\n - " + customer.GetName() + " (" + Format(customer.GetNumberOfAccounts(), "account") + ")";
            return summary;
        }

        //Make sure correct plural of word is created based on the number passed in:
        //If number passed in is 1 just return the word otherwise add an 's' at the end
        private string Format(int number, string word)
        {
            return number.ToString() + " " + (number == 1 ? word : string.Concat(word, "s"));
        }

        public decimal TotalInterestPaid()
        {
            return customers.Sum(customer => customer.GetTotalInterestEarned());
        }

        public string GetFirstCustomer()
        {
            try
            {
                Customer customer = customers.FirstOrDefault();
                return customer?.GetName();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return "Error";
            }
        }
        #endregion
    }
}

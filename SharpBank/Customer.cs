﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace SharpBank
{
    public class Customer
    {
        private readonly string _name;
        private readonly IList<IAccount> _accounts;


        public Customer(string name)
        {
            _name = name;
            _accounts = new List<IAccount>();
        }

        public string GetName()
        {
            return _name;
        }

        public Customer OpenAccount(IAccount account)
        {
            _accounts.Add(account);
            return this;
        }

        public int GetNumberOfAccounts()
        {
            return _accounts.Count;
        }

        public decimal GetTotalInterestEarned()
        {
            return _accounts.Sum(a => a.AccountInterestEarned());
        }

        /*******************************
         * This method gets a statement
         *********************************/
        public string GetStatement()
        {
            var statement = new StringBuilder();

            statement.AppendLine($"Statement for {_name}");

            decimal total = 0.0M;

            foreach (var account in _accounts)
            {
                statement.Append($"\n{account.AccountStatement()}\n");
                total += account.AccountBalanceTotal();
            }

            statement.AppendFormat("\nTotal In All Accounts ${0:N2}", total);

            return statement.ToString();
        }

        [Obsolete("Not used anymore--take out in future")]
        private string StatementForAccount(Account a)
        {
            string s = "";

            //Translate to pretty account type
            switch (a.AccountType())
            {
                case Account.CHECKING:
                    s += "Checking Account\n";
                    break;
                case Account.SAVINGS:
                    s += "Savings Account\n";
                    break;
                case Account.MAXI_SAVINGS:
                    s += "Maxi Savings Account\n";
                    break;
            }

            //Now total up all the transactions
            decimal total = 0.0M;
            foreach (Transaction t in a.Transactions)
            {
                s += "  " + (t.Amount < 0 ? "withdrawal" : "deposit") + " " + ToDollars(t.Amount) + "\n";
                total += t.Amount;
            }
            s += "AccountBalanceTotal " + ToDollars(total);
            return s;
        }

        private string ToDollars(decimal d)
        {
            return string.Format("${0:N2}", Math.Abs(d));
        }

        public void TransferMoney(Account fromAccount, Account toAccount, decimal amount)
        {
            if (fromAccount == null)
            {
                throw new ArgumentNullException("'From' Account");
            }
            if (toAccount == null)
            {
                throw new ArgumentNullException("'To' Account");
            }
            if (amount <= 0)
            {
                throw new ArgumentException($"Invalid amount ({amount})");
            }

            if (!HasAccount(fromAccount))
            {
                throw new ArgumentException("'From' Account does not belong to Customer");
            }
            if (!HasAccount(toAccount))
            {
                throw new ArgumentException("'To' Account does not belong to Customer");
            }

            fromAccount.Withdraw(amount);       // will check if amount > total
            toAccount.Deposit(amount);
        }

        private bool HasAccount(Account account)
        {
            // due to lack of identifiers, using the type as only identifier
            return _accounts.Any(oThisAccount => oThisAccount.AccountType() == account.AccountType());
        }
    }
}

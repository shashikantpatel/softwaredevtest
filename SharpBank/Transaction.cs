﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        public readonly decimal Amount;

        //make accessible for calculations such as last 10 days transactions
        public readonly DateTime TransactionDate;

        public Transaction(decimal amount)
        {
            Amount = amount;
            TransactionDate = DateProvider.GetInstance().Now();
        }

        //Adding an option for date setting, for testing purposes
        public Transaction(decimal amount, DateTime date)
        {
            Amount = amount;
            TransactionDate = date;
        }
    }
}
